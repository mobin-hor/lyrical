import React from 'react';

import Index from './components/layout/Index'
import Lyrics from './components/tracks/Lyrics'

import TrackState from './context/TrackState'

import history from './history';
import {BrowserRouter as Router , Route , Switch} from 'react-router-dom'

import './App.css';

function App() {
  return (
      <Router history={history}>
        <TrackState>
            <>           
                  <Switch>
                      <Route exact path="/" component={Index}></Route>
                      <Route exact path="/lyrics/:id" component={Lyrics}></Route>
                  </Switch>
            </>
        </TrackState>
      </Router>
  );
}

export default App;
