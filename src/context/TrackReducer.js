 import {GET_TOP_TRACKS , SEARCH_TRACKS , LOADING , SET_CURRENT , SET_ENTERANCE} from './types'






export default (state , action)=>{
    switch(action.type){

        case GET_TOP_TRACKS: return{
            ...state,
            TopTracks : action.payload
        }
        case SEARCH_TRACKS:return{
            ...state,
            SearchingTracks : action.payload
        }
        case SET_CURRENT:return{
            ...state,
            curTrack:action.payload
        }
        case LOADING:return{
            ...state,
            loading : !state.loading
        }
        case SET_ENTERANCE:return{
            ...state,
            entrance : true
        }
        default: return{...state}
    }
}