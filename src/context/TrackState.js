import React , {useReducer} from 'react'
import TrackReducer from './TrackReducer'
import TrackContext from './TrackContext'
import axios from 'axios'




const TrackState = props=>{
    const initialState={
        TopTracks:[],
        SearchingTracks:[],
        curTrack:null,
        entrance:false,
        loading:false,
    }


    const [state , dispatch] = useReducer(TrackReducer , initialState)




    const GetTopTracks = async ()=>{
        await axios
        .get(
          `https://cors-anywhere.herokuapp.com/http://api.musixmatch.com/ws/1.1/chart.tracks.get?page=1&page_size=10&country=us&f_has_lyrics=1&apikey=${
            process.env.REACT_APP_MM_KEY
          }`
        )
        .then(res => {
          dispatch({
            type:"GET_TOP_TRACKS",
            payload:res.data.message.body.track_list
          })
        })
        .catch(err => {
          alert("بدلیل استفاده از ای-پی-آی مستقل ، در انجام درخواست مشکل بوجود آمده است ، صفجه برای شما مجددا بارگذاری خواهد شد")
          setTimeout(() => {
            window.location.reload(false);
          }, 100);
        });
    }


    const SearchTrack = async (queries)=>{
      dispatch({
        type:"LOADING"
      })
      await axios
      .get(
        `https://cors-anywhere.herokuapp.com/http://api.musixmatch.com/ws/1.1/track.search?q_artist=${queries.music_artist}&q_track=${queries.music_name}&page_size=5&page=1&s_track_rating=desc&apikey=${
          process.env.REACT_APP_MM_KEY
        }`
      )
      .then(res => {
        dispatch({
          type:"LOADING"
        })
        dispatch({
          type:"SEARCH_TRACKS",
          payload:res.data.message.body.track_list
        })
      })
      .catch(err => {
        alert("بدلیل استفاده از ای-پی-آی مستقل ، در انجام درخواست مشکل بوجود آمده است ، صفجه برای شما مجددا بارگذاری خواهد شد")
        setTimeout(() => {
          window.location.reload(false);
        }, 100);
      });
  }


  const setCurrent = (track)=>{
      dispatch({
        type:"SET_CURRENT",
        payload:track
      })
  }


  const setEntrance = ()=>{
    dispatch({
      type:"SET_ENTERANCE"
    })
  }








    return <TrackContext.Provider
        value={{
            TopTracks:state.TopTracks,
            SearchingTracks: state.SearchingTracks,
            loading:state.loading,
            curTrack:state.curTrack,
            entrance:state.entrance,
            GetTopTracks,
            SearchTrack,
            setCurrent,
            setEntrance
            
        }}>
    {props.children}
    </TrackContext.Provider>

}


export default TrackState