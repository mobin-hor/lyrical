import React,{useState , useContext} from 'react'
import TrackContext from '../../context/TrackContext'

 const Search = () => {


    const trackContext = useContext(TrackContext)


    const [search , setSearch] = useState({
        music_name:'',
        music_artist:'',
    })

    const {music_artist , music_name} = search


    const inputChange = (e)=>{
        setSearch({...search , [e.target.name]:e.target.value})
    }


    
    const showSearch = ()=>{
        document.getElementById("searchBar").firstChild.classList.toggle('show')   

        if(search.music_artist!=='' || search.music_name !==''){
            trackContext.SearchTrack(search)
        }
        setSearch({
            music_name:'',
            music_artist:'',
        })
    }

    return (
        <>
            <button onClick={showSearch} className="btn btn-block align-items-center">
                <i className="fas fa-search"></i>
            </button>
            <div id="searchBar">
            <form class="form">
                <div class="form-group">
                    <input onChange={inputChange} type="text" name="music_name" class="form-control" id="musicname" placeholder="نام موسیقی" value={music_name}/>
                </div>
                <div class="form-group mb-2">
                    <input onChange={inputChange} type="text" name="music_artist" class="form-control" id="musicartist" placeholder="نام خواننده" value={music_artist}/>
                </div>
            </form>
        </div>
        </>
    )
}


export default Search