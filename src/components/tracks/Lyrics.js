import React,{useState , useEffect , useContext} from 'react'
import TrackContext from '../../context/TrackContext'
import Loader from '../Loader'
import axios from 'axios'

import guitar from '../../images/MainBack2.jpg'


 const Lyrics = ({match}) => {

    const [lyric , setLyric] = useState('')
    
    const trackContext = useContext(TrackContext)
    const {curTrack} = trackContext


    useEffect(()=>{
        axios.get(
          `https://cors-anywhere.herokuapp.com/http://api.musixmatch.com/ws/1.1/track.lyrics.get?track_id=${match.params.id}&apikey=${
            process.env.REACT_APP_MM_KEY
          }`
        )
        .then(res => {
            setLyric(res.data.message.body.lyrics.lyrics_body)
        })
        .catch(err => console.log(err));
    },[])


    return (
        <div id="lyric">
          <img src={guitar} alt=""/>
          <p><span>{curTrack.artist_name}</span> کاری از  <i className="fas fa-microphone-alt"></i></p>
          <p><span>{curTrack.track_name}</span>  با نام <i className="fas fa-music"></i></p>
          <p><span>{curTrack.album_name}</span>  از آلبوم  <i className="fas fa-compact-disc"></i></p>
          
          {lyric==='' ? <Loader></Loader> : <p className="text">{lyric}</p>}
        </div>
    )
}


export default Lyrics