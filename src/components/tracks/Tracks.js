import React,{useContext , useEffect} from 'react'
import TrackContext from '../../context/TrackContext'
import Track from './Track'
import Loader from '../Loader'


 const Tracks = () => {


    const trackContext = useContext(TrackContext)
    const {TopTracks ,  GetTopTracks , SearchingTracks}  = trackContext

    useEffect(() => {
        GetTopTracks();
      },[]);


    return (
        <div className="tracks row">
          {SearchingTracks.length===0 ? (TopTracks.length===0? <Loader></Loader> : TopTracks.map(el=>(
                <div key={el.track.track_id}  className="col-md-6">
                  <Track track={el.track}></Track>
                </div>)
            )) : (SearchingTracks.map(el=>(
                <div key={el.track.track_id}  className="col-md-6">
                  <Track track={el.track}></Track>
                </div>)
            ))}

        </div>
    )
}


export default Tracks