import React,{useContext} from 'react'
import {Link} from 'react-router-dom'
import TrackContext from '../../context/TrackContext'

 const Track = ({track}) => {

    const trackContext = useContext(TrackContext)


    const setCur = ()=>{
        trackContext.setCurrent(track)
    }

    return (
        <div className="card mb-2">
            <Link onClick={setCur} style={{textDecoration:'none' , color:'black'}} to={`/lyrics/${track.track_id}`}>
                <div className="card-body">
                     <h5 className="card-title"> {track.track_name}</h5>
                    <h6 className="card-subtitle mb-2 text-muted"><i className="fas fa-microphone-alt"></i> {track.artist_name}</h6>
                </div>
            </Link>
        </div>
    )
}


export default Track