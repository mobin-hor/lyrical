import React,{useEffect , useContext} from 'react'
import Tracks from '../tracks/Tracks'
import Search from '../tracks/Search'
import TrackContext from '../../context/TrackContext'

import enterance from '../../images/entrance.jpg'
import mainback from '../../images/MainBack.jpg'

import {useSpring, animated} from 'react-spring'
import {Parallax, ParallaxLayer} from 'react-spring/renderprops-addons'



const Index = () => {


    const trackContext = useContext(TrackContext)
    const {setEntrance , entrance} = trackContext

    useEffect(()=>{
        setTimeout(() => {
            setEntrance();
        }, 2500);
    },[])



    const props = useSpring({from:{opacity:0} , to:{opacity:1}})


    return (
        <div  id="main">
            {entrance ? <animated.div style={props}>

                        <Parallax>
                            <img className="img-fluid" src={mainback} alt=""/>

                            <ParallaxLayer offset={1.5} speed={-0.9}>
                                <i className="text-danger float-left fas fa-music"></i>
                            </ParallaxLayer>
                            
                            <div className="contain">
                                <Search></Search>
                                <ParallaxLayer offset={1} speed={-1}>
                                <i className="text-danger text-center fas fa-music"></i>
                                </ParallaxLayer>
                                <Tracks></Tracks>
                            </div>
                            <ParallaxLayer offset={0} speed={1}>
                                <i className="text-danger float-right fas fa-music"></i>
                            </ParallaxLayer>
                        </Parallax>
                    </animated.div> :  
                    
            <img id="enterance" className="img-fluid" src={enterance} alt=""/> }
            
        </div>
    )
}


export default Index