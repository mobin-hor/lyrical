import React from 'react'

 const Loader = () => {
    return (
        <div className="loader">
            <div className="spinner-border text-danger m-auto text-center d-block" role="status">
                <span className="sr-only">در حال بار گذاری...</span>
            </div>
        </div>
    )
}


export default Loader